---
title: "Week 4. Data--Project, Open, Taiwan"
date: 2022-10-03T11:25:30+08:00
draft: false
categories: [note, team, data, game]
tags: [project, data, overview]
---
<img src="https://ksr-ugc.imgix.net/assets/034/214/064/02129c76512907ef6697d991997cce72_original.png?ixlib=rb-4.0.2&crop=faces&w=1552&h=873&fit=crop&v=1626328496&auto=format&frame=1&q=92&s=dbb30b7ea348b87b1d86f6dd543a2ebf" style="zoom:50%;" />

## 關於 project 

* 問題/動機 (Question/Motivation)：你有興趣/別人也可能有興趣的問題
* 背景 (Background)：大家現在知道什麼？有什麼想知道，但還不知道的？
* 資料 (Data)：相關的資料與可能回答的問題
* 探討/研究 (Formulation and Investigation [Exploratory Data Analysis, Data Visualization, Modelling, Estimation/Prediction and Machine Learning Model])：過去發生了什麼？這些變數之間的關聯？在實際面上的意義？
* 結論 (Conclusion and Discussion)：我們發現了什麼？這些發現解釋與應用的範圍與限制，後續的可能方向
## Project data

現在網路上資料很多，應該有妳需要或適合做不同等級的資料。但你要找到適當的**關鍵字**來搜索，另外則**篩選**。另外也需要閱讀一些文件、報告、paper。當然，其中幾乎大多數都是英文文件。習慣就好。以下是我搜尋 "data for student projects", "data for data science projects" 的不錯連結。我是以[DuckDuckGo](https://duckduckgo.com/) 搜尋。

* [Data Science Career Paths: Different Roles](https://www.springboard.com/blog/data-science-career-paths-different-roles-industry/)

* [Top 5 Data Science Projects with Source Code to kick-start your Career](https://data-flair.training/blogs/data-science-projects-code/)

* [70+ Machine Learning Datasets & Project Ideas – Work on real-time Data Science projects](https://data-flair.training/blogs/machine-learning-datasets/)

* [16 Data Science Project Ideas with Source Code to Strengthen your Resume](https://data-flair.training/blogs/data-science-project-ideas/) 

* Kaggle: [csv datasets](https://www.kaggle.com/datasets?fileType=csv), [datasets](https://www.kaggle.com/datasets)

* Import/Read csv to R/Rstudio: [R](https://www.geeksforgeeks.org/reading-the-csv-file-into-dataframes-in-r/), [Rstudio](https://support.rstudio.com/hc/en-us/articles/218611977-Importing-Data-with-the-RStudio-IDE)

  

* [18 Places to Find Free Data Sets for Data Science Projects](https://www.dataquest.io/blog/free-datasets-for-projects/)

* [19 Free Public Data Sets for Your Data Science Project](https://www.springboard.com/blog/free-public-data-sets-data-science-project/)

* [25+ websites to find datasets for data science projects](https://www.analyticsvidhya.com/blog/2016/11/25-websites-to-find-datasets-for-data-science-projects/)

* [50+ free Datasets for Data Science Projects](https://blog.journeyofanalytics.com/50-free-datasets-for-data-science-projects/)

* [6 Complete Data Science Projects](https://www.springboard.com/blog/data-science-projects/). 文尾 *Tips for Creating Cool Data Science Projects* 很值得參考。

## Open Data

* [政府開放資料平台](https://data.gov.tw/)
* [台北市資料大平台](https://data.taipei/)
* [Data Portals](http://dataportals.org/)  A Comprehensive List of Open Data Portals from Around the World
* [Open Data Cube](https://www.opendatacube.org/) The Open Data Cube (ODC) is an Open Source Geospatial Data Management and Analysis Software project that helps you harness the power of Satellite data.

## Taiwan

* [Oceans of Data — g0v.tw and Taiwan’s Open Data Model](https://medium.com/open-and-shut/oceans-of-data-g0v-tw-and-taiwans-open-data-model-6a27f3203b01)

* [Global Open Data Index of Taiwan](https://index.okfn.org/place/)
* [How does a country get to open data? What Taiwan can teach us about the evolution of access](https://www.niemanlab.org/2013/04/how-does-a-country-get-to-open-data-what-taiwan-can-teach-us-about-the-evolution-of-access/)

### Reproducible Science
* [Reproducible Science](https://reproduciblescience.org/)
* [Reproducibility (wiki)](https://en.wikipedia.org/wiki/Reproducibility)

