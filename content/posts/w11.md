---
title: "R Games League of Legends"
date: 2022-11-19T23:46:39Z
draft: false
tags: [adm, team, presentation]
---
<img src="https://www.gameshub.com/wp-content/uploads/sites/5/2022/08/2021_Key_art.jpg?resize=2048,1152" style="zoom: 33%;" />

# 期末報告 (課堂報告) Starting 12/7*

＊ prop=proposal, qsc=questions, suggestions and comments.
### 報告順序  (NEW)
5 12  7  4 10  8 11 15 17 16 18 13  9 20  2 14 19  1  3  6.

請提早準備。



1. ([prop](https://docs.google.com/document/d/1ejSQtG5ae1lttXa-Xe4Z22a__9U6pGERTRdU4Z3OGuc/edit?usp=drivesdk), [pres](), [rept](), [qsc](https://classroom.google.com/c/MzkxOTcwNDMyODk5/sa/NDUwMTk5NTY4Mzky/details)): 廖宸育,李柏霖,黃寬勉,江陳洤 p
2. 排球擊球高度與攻擊技巧的相關分析 ([prop](https://docs.google.com/document/d/1kx5VLPy03hpaozVuosXVPvpKazHIhnYmKFzyoN1Z5mk/edit), [pres](), [rept](), [qsc]()) .統卵互助會: 紀懷佾, 吳俞憲, 黃禾崴, 張耘睿 o
3. 外送 ([prop](https://docs.google.com/document/d/1JZ8FKUHykoKOMoEse9Be-iOantLgsOls/edit), [pres](), [rept](), [qsc]()): 223: 洪福臨, 張棋鈞, 林軒宏, 江柏緯, p
4. 地震 ([prop](https://docs.google.com/document/d/1-HVam_w-IMdvpHABg1mFCyNxGiqJTnJ-/edit), [pres](), [rept](), [qsc]()): 柯拓宇,鐘政紘,林宏宗 p
5. 影響台灣生育率之可能 ([prop](https://docs.google.com/document/d/18lWQ1en1m-8z9YxZCk3a2S2CHHN6yP4O/edit), [pres](), [rept]()): 黃奕嘉,陳威辰,黃鈺翔,俞宗瑋 p
6. NBA 球員薪資分析 ([prop](https://drive.google.com/file/d/1-hWQWeC2PFv_7duXy4LfUAKtu7xKj3eo/view), [pres](), [rept](), [qsc]()) Golden State Warriors: 林伯叡, 陳政博, 王宥人, 陳重佑, 鄭宗禾 p
7. Blackjack ([prop](https://docs.google.com/document/d/1ElyUoxSVyVctrv2MRv3YTxZKUTqyAs4U/edit), [pres](), [rept](), [qsc]()) Predict Expert: 陳力福, 胡成詳, 陳兆維 p
8. 重大車禍分析 ([prop](), [pres](), [rept](), [qsc]()) 三人小隊: 楊耀鈞, 江庭育, 謝君妮 （New Proposal?）
9. 台北市住宅竊盜點位 ([prop](https://docs.google.com/document/d/16YhcOWkGt5WbPSVxNTR5-DG6lRkGf1TB2PKgsRxX8Kc/edit), [pres](), [rept](), [qsc]()) 分數不太隊: 楊廣理, 黃煜霖, 高祺翔, 呂權祐, 林家右 O
10. iphone在台灣的持有比率 ([prop](https://365ndhu-my.sharepoint.com/:w:/g/personal/411011228_o365_ndhu_edu_tw/ETy9j5Ss9hhIucwuK3anlDkBt1034_hGG6wvWqmVb-IuUA?e=4%3ACvuk8l&at=9), [pres](), [rept](), [qsc]()) 分數給我好嗎: 高少騏, 楊子寬, 林根玄 p
11. 海洋汙染 ([prop](https://docs.google.com/document/d/1xiBcbFCrZJ4bDCuwd9Sac83vG1Wka_3KqDCC2Ztfp9w/edit), [pres](), [rept](), [qsc]()) 地球保衛隊: 汪子瑄, 王亭懿, 陳亭妤, 鍾欣頻, 林語柔 p
12. Stephen Curry as a Final MVP? ([prop](https://docs.google.com/document/d/1sp-IBDRJD9HCxs2XA7UJeu4iJ49syJLE/edit), [pres](), [rept](), [qsc]()) 拜託別當我: 洪梓恩, 林畊佑, 黃明哲 p
13. 臺灣國球——中華職棒背後的數據 ([prop](https://drive.google.com/drive/folders/1Zif5Pfv8948UcWXQj-i0N6WwNzUC3Ccs), [pres](), [rept](), [qsc]()) 東華棒球隊: 李世勛, 黃定綸, 李志皓, 葉翰東 p
14. 電影票房情況和來源國家分析 ([prop](https://docs.google.com/document/d/1en4lm6jLa4LNZ4HPeQyr_U9ibcEHSa_Q/edit), [pres](), [rept](), [qsc]()) 東華電影調查組: 李奕磊, 蔡念秤 p
15. 森林動態樣區之每木調查 ([prop](https://365ndhu-my.sharepoint.com/personal/410911339_o365_ndhu_edu_tw/_layouts/15/onedrive.aspx?id=%2Fpersonal%2F410911339%5Fo365%5Fndhu%5Fedu%5Ftw%2FDocuments%2F%E7%B5%B1%E8%A8%88%E8%BB%9F%E9%AB%94%E8%88%87%E5%AF%A6%E5%8B%99%E6%87%89%E7%94%A8%E5%88%86%E7%B5%84%E5%A0%B1%E5%91%8A%2Epdf&parent=%2Fpersonal%2F410911339%5Fo365%5Fndhu%5Fedu%5Ftw%2FDocuments&ga=1), [pres](), [rept](), [qsc]()) 熊熊: 高翌哲, 王弘奇, 李宛芸, 杞宥頡, 李永仁 p
16. Youtube使用者喜歡看甚麼類型的片片 ([prop](https://docs.google.com/document/d/1lbImOAdU7gk4pKDJilm2M4NYtyguvmUf/edit), [pres](), [rept](), [qsc]()) 狗狗統軟特戰隊: 李郁青, 廖廣筑, 鄒嘉霖 p
17. Youtube 的趨勢 ([prop](https://drive.google.com/file/d/1mSBidEcrrxciNIDqAt96pLP9BC3-Qm87/view), [pres](), [rept](), [qsc]()) 香蕉不好吃: 王婕如, 陳郁晴 p
18. 109年花蓮縣公告地價及公告土地現值 ([prop](https://docs.google.com/document/d/1g2xIKgRA7AMTkwWXfG3exDRDjg81bf6cqrhPtBCA7jw/edit), [pres](), [rept](), [qsc]()) 麗春紅茶: 黃筱嵐,蔡佩珊, 李亭儀 O
19. 近年來 YT 頻道未來趨向 ([prop](https://drive.google.com/file/d/1bb8C6csxHUkyH2B_pTCaX26EGSUOHjbh/view), [pres](), [rept](), [qsc]()) 王振安, 林鈺翔 p
20. ([prop](), [pres](), [rept](), [qsc]()): 潘世龍,李冠頡,林子育,林子硯, 鍾旻鈞 ?

#### 書面報告

TBA: 韓仕毅, 楊祥弘, 李銘岱, 古智忠, 黃偉軒, 姚忠驛, 陳子嘉, 張晉嘉, 呂采珊, 鍾旻鈞, 賴聖哲

* 個人

* 團隊

  * 17. Youtube 的趨勢 ([prop](https://drive.google.com/file/d/1mSBidEcrrxciNIDqAt96pLP9BC3-Qm87/view), [pres](), [rept](), [qsc]()) 香蕉不好吃: 王婕如, 陳郁晴 p

  * 8. 重大車禍分析 ([prop](), [pres](), [rept](), [qsc]()) 三人小隊: 楊耀鈞, 江庭育, 謝君妮 （New Proposal?）

       


### 說明

* **課堂報告**：時間原則 x min, 硬體/Q&A: 3 min.。參考 Rmarkdown 模例[html](https://chtsao.gitlab.io/krg20/lalabear.html), [rmd](https://chtsao.gitlab.io/krg20/lalabear.Rmd)。Youtube 可能。
* **書面報告**：(2022) x/x 23：59 前寄至我電郵信箱，以 pdf, html 檔為偏好格式。[架構參考](https://chtsao.gitlab.io/krg20/proj.template.nb.html)。
* 不在編組名單內小組/同學以繳交書面/電子報告為原則。如有高度課堂報告意願請儘快與我聯繫。如上列8小組沒有課堂報告意願，也請在表訂報告7天前告知，以便安排。
* （新）Kno 狗尾續貂, 試著幫已報告各組加上標題, 如果不適合或有另外更好建議，請讓我知道修正。另外，徵求各組同意後，我會將課堂報告以及最後書面報告放在課網連結，提供有興趣的閱者參考。

### R Markdown Links (New)

* [Lgatto's slide template](https://github.com/lgatto/slide-templates)
* [uslides by mkearney](https://github.com/mkearney/uslides)
* [R Markdown ioslides presentation](https://bookdown.org/yihui/rmarkdown/ioslides-presentation.html) from [R Markdown: The Definitive Guide](https://bookdown.org/yihui/rmarkdown/)
* [Gallery from Rstudio](https://rmarkdown.rstudio.com/gallery.html)
* [Steve's R Markdown Templates](https://github.com/svmiller/svm-r-markdown-templates)

### 共同提醒與建議
* 一般
  * 提早準備，提前練習；自己/組內/找聽眾 Re 個幾次，注意流暢度，轉接，大約時間，節奏
  * 提早到場，確定硬、軟體相容性，電腦、麥克風、音響與其效果

* 內容
  * 聚焦：以 project 架構思考--在時間/人力/腦力限制下，研選/嚴選分析的主題探究程度與重點
  * 開始的問題提出，提示整個報告的重點；最後結論回應對問題的回答。
* 統計
  * EDA $\neq$ plot(data.frame) 或資料表列。以資料報告來說，比較更像一個初步資料呈現/summary. 
  * Raw data is more informative. 在 scatterplot 時，盡量做原始資料呈現，不先做 data summary (如僅畫平均的點)。
  * 統計/機器學習等方法可處理$Y$與多變數如$X_1, \cdots, X_p$之間的關係，不要僅限於單變數或簡單線性迴歸。 
  * Pie chart 不太適合作為比較或變化的呈現。比較的呈現要注意表格或統計圖形的挑選。
  * Scatterplot 無法以有效的統計模型說明時，也可回到統計圖表的根本，看圖理解，探尋可能的（不同）關聯。如 Alberto Cairo 建議，可以將圖形透過十字切割架構，劃分為類似於象限I,II, III, IV的四區，分別討論。更為進階的作法，可以透過如 active t-SNE 的方式，先將資料視覺化(二維圖)，再探究群內意義，群間關聯。
* 呈現
   * 精簡：少字大字，一張投影片一個主題（為原則）
   * 圖形的座標要清楚，事先思考選擇 X-Y軸座標
   * 避免不必要的翻頁，特別是連續翻多頁。如有必要，事先做好跳頁的設定。
   * 轉接頭等硬體設定事先準備確定 OK (可以課前先和系辦借)
   
     