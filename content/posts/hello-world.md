---
title: "Hello World"
date: 2022-09-05T21:39:30+08:00
draft: false
categories: [note]
---

<img src="https://www.schoolofdatascience.amsterdam/wp-content/uploads/2017/07/19989649_1487790424576061_5172679695012103031_n-2.jpg" style="zoom:50%;" />

## 歡迎！

* [課網](https://chtsao.gitlab.io/rgame22/)
* [GoogleClassRoom](https://classroom.google.com/c/NTQ1ODMyNDk2ODM1?cjc=o7in35f)
* [Google Meet](https://meet.google.com/ouy-xvtc-nrp)
* 課程時間：Mon. **1410**-1500, Wed. **1510**-1700 @ SE B101 (3D)/ GoogleMeet (Virtual)
* [About](https://chtsao.gitlab.io/rgame22/about/)

