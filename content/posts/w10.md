---
title: "R eSources"
date: 2022-11-14T11:04:06Z
draft: false
tags: [references]
---

<img src="https://img.4gamers.com.tw/ckfinder/files/Elvis/News/20190723-Pokemon/TeamGORocket-invasion-2019.jpg" style="zoom:50%;" />

## 小心/提醒

### Aleph

* Sources available in R and Rstudio: ?xxx, help.start(); help at pane IV. 
* [Contributed Documentation](https://cran.r-project.org/other-docs.html)

## R Books and Links

### Books

1. [R for Data Science](https://r4ds.had.co.nz/) (bookdown): Intro, demo, codes for  data wrangling, EDA, prelim modelling, communications using ${\tt tidyverse}$, ${\tt ggplot2}$ packages. **Highly recommended!** You will get a good overview picture of R/Rstudio/DS workflow after reading Chapter 1 to Chapter 10. 

2. [R Cookbook](https://rc2e.com/) (bookdown). Book 1, 2  are of [O'Reilly Books](https://www.oreilly.com/products/books-videos.html). [Know these animals](https://www.oreilly.com/animals.csp).

3. [The Art of R Programming](http://heather.cs.ucdavis.edu/%7Ematloff/132/NSPpart.pdf) (pdf)
   
   <img src="https://www.oreilly.com/content/wp-content/uploads/sites/2/2019/06/primates-slender-loris-in-waking-sleeping-antique-print-1893-51148-p-8e88ca04dae772621e0068327d53a9ea.jpg" style="zoom:25%;" />

4. [Advanced R](https://adv-r.hadley.nz/) (bookdown). As the title indicates, this is for more advanced R users.

5. [Introduction to Data Science](https://rafalab.github.io/dsbook/).(bookdown) Growing out of class notes of HarvardX [Data Science Series](https://www.edx.org/professional-certificate/harvardx-data-science)

#### Courses and Portals

1. [732A50 Advanced Programming in R @ilu.se ](https://www.ida.liu.se/~732A50/info/courseinfo.en.shtml): course web, **great** collection of R online resources links.
2. [Basic Statistical Analysis Using the R Statistical Package@BUMC](http://sphweb.bumc.bu.edu/otlt/MPH-Modules/BS/R/R-Manual/R-Manual-TOC.html): Intro to R + Stat methods/codes 
3. [Stat 545.com](https://stat545.com/), [Stat 547 at UBC](https://stat545.stat.ubc.ca/). Less MI but more others (Recall OCEMIC).

#### More

1. [R Book List](http://freecomputerbooks.com/langRBooks.html) from [FreeComputerBooks.com](http://freecomputerbooks.com/)
2. R packages portals (under Construction): [Searching for R packages](https://rviews.rstudio.com/2018/10/22/searching-for-r-packages/), [leaflet package](https://rstudio.github.io/leaflet/), [It's crantastic!](https://crantastic.org/)
3. Rstudio guys: [Hadley Wickham](http://hadley.nz/) (tidyverse, etc). [Yihui Xie] (https://yihui.org/) (bookdown, etc).
4. Github/Gitlab resources
   * [awesome-r](https://github.com/iamericfletcher/awesome-r-learning-resources)
   * [Another AwesomeR](https://github.com/ktaranov/AwesomeR) 
5. [Big Book of R](https://www.bigbookofr.com/index.html)
