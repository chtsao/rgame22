---
title: "About"
date: 2022-09-06T15:24:16+08:00
tags: []
categories: [admin]
---
<img src="https://p2.bahamut.com.tw/B/2KU/84/3cf8118c5ad428f499f3ec176c1fuqc5.JPG" alt="孔明" style="zoom:50%;" />

### 初始設定

這是統計軟體與實務應用2022課程網頁。       Curator：Kno Tsao

資料的取得、面向與規模隨著網路發展超速成長。統計是資料科學中的一個重要主軸。本課程將透過引介統計軟體 R, Rstudio, 以及網站 R bloggers, Github, Kaggle 引發學生在資料科學分析與實作的第一步。教學計畫表 syllabus 部份的資料與內容可能將視課程進行狀況略做調整。

以上是官方的說法。真實的設定是  ——— **R games**. 

有興趣同學也可參考一下[2021版課網](https://chtsao.gitlab.io/rgame21/)。


### 課程資訊

* [Syllabus](https://chtsao.gitlab.io/rgame22/rgame22.sylla.pdf). Curator: C. Andy Tsao (Kno).  Office: SE A411.  Tel: 3520
* Lectures: 
  * 3D: Mon. **1410**-1500, Wed. **1510**-1700 @ SE B101.
  * Virtual: [GoogleClassRoom](https://classroom.google.com/c/NTQ1ODMyNDk2ODM1?cjc=o7in35f), [Google Meet](https://meet.google.com/ouy-xvtc-nrp)
* Office Hours:  Mon. 15:10-16:00, Thr. 15:10-16:00. @ SE A411 or by appointment. (Mentor Hour:  Tue/Thr 1200-1300. 也歡迎來，但若有導生來，則導生優先)
* TA Office Hours: (NEW)
  * [呂一昕](mailto:610911007@gms.ndhu.edu.tw): Thr. 17:00-19:00 @ SE A412
  * [蘇羿豪](mailto:611011102@gms.ndhu.edu.tw): Tue. 15:00-16:00, Thr. 17:00-18:00.  @SE A311

* Prerequisites: Intro to Probability, Statistics (taken or taking) and **curiosity about data/world/parallel worlds**.

