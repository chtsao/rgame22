---
title: "Voyage"
date: 2022-12-26T12:00:49+08:00
draft: false
tags: [adm, team, project, presentation]
---
![](http://www.birds.cornell.edu/bbimages/clo/images/wedo/themes/theme_research_valley.jpg)

## 公告

期末報告結束之後，R games 告一段落。**即日起沒有原形式之上課**。但 Game master Kno 會繼續 PO 一至兩次 內容，提供 有興趣的玩家探索。敬請期待！

## (重要) 期末報告 (書面報告)

未參加小組課堂報告同學請務必繳交（上傳）書面報告。

* 個人：韓仕毅, 楊祥弘, 李銘岱, 古智忠, 黃偉軒, 姚忠驛, 陳子嘉, 張晉嘉, 呂采珊, 鍾旻鈞, 賴聖哲 等
* 團隊
  * 19. 近年來 YT 頻道未來趨向 ([prop](https://drive.google.com/file/d/1bb8C6csxHUkyH2B_pTCaX26EGSUOHjbh/view), [pres](), [rept](), [qsc]()) 王振安, 林鈺翔 p
  * 20. ([prop](), [pres](), [rept](), [qsc]()): 潘世龍,李冠頡,林子育,林子硯, 鍾旻鈞 ?
  * 4. 地震 ([prop](https://docs.google.com/document/d/1-HVam_w-IMdvpHABg1mFCyNxGiqJTnJ-/edit), [pres](), [rept](), [qsc]()): 柯拓宇,鐘政紘,林宏宗 p
  * 17. Youtube 的趨勢 ([prop](https://drive.google.com/file/d/1mSBidEcrrxciNIDqAt96pLP9BC3-Qm87/view), [pres](), [rept](), [qsc]()) 香蕉不好吃: 王婕如, 陳郁晴 p
  * 11. 海洋汙染 ([prop](https://docs.google.com/document/d/1xiBcbFCrZJ4bDCuwd9Sac83vG1Wka_3KqDCC2Ztfp9w/edit), [pres](), [rept](), [qsc]()) 地球保衛隊: 汪子瑄, 王亭懿, 陳亭妤, 鍾欣頻, 林語柔
  * 未參加課堂報告其他小組
### 說明

* **課堂報告**：時間原則 25 min, 硬體/Q&A: 3 min.。參考 Rmarkdown 模例[html](https://chtsao.gitlab.io/krg20/lalabear.html), [rmd](https://chtsao.gitlab.io/krg20/lalabear.Rmd)。Youtube 可能。
* **書面報告**：(2023) 1/10 11：59 前上傳至
  [GoogleClassRoom](https://classroom.google.com/c/NTQ1ODMyNDk2ODM1?cjc=o7in35f)，以 pdf, html 檔為偏好格式。[架構參考](https://chtsao.gitlab.io/krg20/proj.template.nb.html)。
* 不在編組名單內小組/同學以繳交書面/電子報告為原則。如有高度課堂報告意願請儘快與我聯繫。如上列8小組沒有課堂報告意願，也請在表訂報告4天前告知，以便安排。
