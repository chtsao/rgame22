---
title: "Techincal Presentation"
date: 2023-01-10T05:18:07+08:00
draft: false
tags: [presentation]
---

<img src="https://uploads.theartofeducation.edu/2018/04/me-2-page-2.jpg" style="zoom:67%;" />

Presentation 是 data science OCEMIC 中 Communication 中最核心的部份。以下是一個我喜歡的架構

* Title: 切題/適當 hook
* Outline
* Motivating Question：主要的動機問題，為何重要（對領域，對研究者，對聽者）？What have been done (Good/Bad)? [懸/破題/Visual  if possible]
* Formulation: 我們如何轉化，式構，來回答這個問題
* Data and Methods: 資料來源/正確性/取得方式；主要使用方法/方法之主要概念/Implementation; 選擇此資料/方法的原因、說明與合理性
* VRV: Visual (pre EDA) +  Results + Visual (post Communication)
* Summary: Recap main results and brief take-home message

* Literature and Links: Include your supplmental materials such as codes in QR or alternatives.

